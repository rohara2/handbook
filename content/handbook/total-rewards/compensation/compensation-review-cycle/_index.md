---
title: Compensation Review Cycle
description: "On this page, we explain how we carry out the Compensation Review Cycle."
---

## Introduction

On this page, we explain how we carry out the Annual Compensation Review (ACR) cycle. For more information on the process and input review for the Compensation Review Process, please see the following [handbook page]({{< ref "review-cycle-inputs" >}}).

If you have any feedback or questions about the compensation review cycle, please contact [People Connect](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).

## Annual Compensation Review

### **All information related to the FY25 Annual Compensation Review cycle will be updated to the handbook by no later than the end of December 2023**

Increases for Compensation Review Cycles are based on:
1. Assessing market changes in the [compensation calculator]({{< ref "compensation-calculator" >}}) inputs
1. Talent Assessments outlining performance in the role.

If there is an increase in Location Factor and/or Benchmark, team members affected will have the new location factors applied to their compensation range, but would not receive an automatic percent increase to their base salary. For example, if the location factor in a region increases by 10% the team member will not receive an automatic 10% adjustment.  The team member will be brought into range after any changes to benchmark, location factor, and exchange rate take place and then their performance increase will be layered on top of this market adjustment.

#### Process overview

```mermaid
graph LR
    start((Annual review<br/>process<br/>kicks off))
    step_manager_review(Manager determines<br/>provisional performance factor<br/>of team members)
    step_exec_review_1(Executive review)
    step_tr_calculate(Total Rewards<br/>team calculates<br/>proposed increases)
    step_manager_workday(Manager review<br/>in Workday)
    step_exec_review_2(Executive review)
    step_manager_final(Manager informs<br/>direct reports of<br/>adjustment to<br/> compensation)
    stop((New<br/>compensation<br/>effective 1<br/>Feb 2023))

    start-->step_manager_review
    step_manager_review-->step_exec_review_1
    step_exec_review_1-->step_tr_calculate
    step_tr_calculate-->step_manager_workday
    step_manager_workday-->step_exec_review_2
    step_exec_review_2-->step_manager_final
    step_manager_final-->stop
```

### Eligibility

Eligible team members for Annual Compensation Review have a hire date on or before:

- October 31st for a cash compensation increase.
- September 5th for an equity refresh. Team members are eligible for a refresh grant if they have been at the company for six months ahead of the grant date cutoff. The grant date is slated to be around mid-March and the cutoff is March 5th.

Team members hired after October 31st will be reviewed to ensure their cash compensation does not fall below the minimum of the updated compensation range. Team members may be below the compensation range due to updates to the range for the upcoming year that would include changes to benchmark, location factor, and/or exchange rate, if applicable. If this does occur, the team member will be adjusted to the minimum of the range during the Annual Compensation Review cycle.
